from pwnedpasswds import __version__
from pwnedpasswds import hash_passwd


def test_version():
    assert __version__ == "0.1.0"


def test_sha1():
    assert hash_passwd("correcthorsebatterystaple") == (
        "BFD36",
        "17727EAB0E800E62A776C76381DEFBC4145",
    )
